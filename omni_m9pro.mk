# Boot animation
TARGET_SCREEN_HEIGHT := 1200
TARGET_SCREEN_WIDTH := 1920

# Inherit some common CyanogenMod stuff
$(call inherit-product, vendor/omni/config/common_tablet.mk)

# Inherit device configuration
$(call inherit-product, device/rockchip/m9pro/full_m9pro.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := omni_m9pro
# Set product name
PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=pipo_m9pro